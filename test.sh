#!/bin/bash
# Simple bash test to sanity-check our container is reacting as expected.
test_ip=1.2.3.4
docker run -d --name fakedns-test -it --rm -p 53 fakedns ${test_ip}
container_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' fakedns-test)
echo "our container is running on IP" ${container_ip}
container_ip="192.168.57.1"
returned_ip=$(dig +short something.other @${container_ip})
echo "result was ${returned_ip} and we expected ${test_ip}"
if [ "${test_ip}" == "${returned_ip}" ]; then
  echo "PASSED."
else
  echo "FAILED."
fi
docker rm -f fakedns-test
