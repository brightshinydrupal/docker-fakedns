FROM frolvlad/alpine-python3

RUN wget https://github.com/pathes/fakedns/raw/master/fakedns.py

ENTRYPOINT [ "python3", "fakedns.py" ]
